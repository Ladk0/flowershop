package Store;

import Store.Model.Product;

import java.util.List;

public class Bouquet implements Product {
    int id;
    List<Plant> plants;

    public Bouquet(int id, List<Plant> plants) {
        this.id = id;
        this.plants = plants;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public double getSellPrice() {
        double price = 0;

        for (Plant plant : plants)
            price += plant.getSellPrice();
        return price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Plant> getPlants() {
        return plants;
    }

    public void setPlants(List<Plant> plants) {
        this.plants = plants;
    }

    void add(Plant plant) {
        plants.add(plant);
    }

    boolean remove(int id) {
        try {
            for (Plant plant : plants) {
                if (plant.id == id)
                    plants.remove(id);
            }
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public double getInPrice() {
        double price = 0;

        for (Plant plant : plants)
            price += plant.getInPrice();
        return price;
    }


}
