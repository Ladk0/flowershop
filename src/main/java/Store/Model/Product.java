package store;

public interface Product {
    int getId();
    double getSellPrice();
    double getInPrice();

}
