package store;

import store.Delivery.Delivery;
import store.Model.Item;
import store.Payment.Payment;

import java.util.List;

public class Order {
    private List<Item> items;
    private Payment payment;
    private Delivery delivery;

    void setPaymentStrategy(Payment payment) {
        this.payment = payment;
    }

    void setDeliveryStrategy(Delivery delivery) {
        this.delivery = delivery;
    }

    double calculateTotalPrice() {
        double price = 0;
        for (Item item : items) {
            price += item.getPrice();
        }
        return price;
    }

    void processOrder() {
        System.out.println("Order closed");
    }

    void addItem(Item item) {
        items.add(item);
    }

    void removeItem(Item item) {
        items.remove(item);
    }
}
