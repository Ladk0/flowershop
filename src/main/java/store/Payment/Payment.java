package store.Payment;

public interface Payment {
    void pay(double price);
}
