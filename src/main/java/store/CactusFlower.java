package store;

import store.Model.Item;

public class CactusFlower extends Item {
  private String forma;
  private double heightSpike;
  private boolean isBloom;

  public CactusFlower(String forma, double heightSpike, boolean isBloom) {
    this.forma = forma;
    this.heightSpike = heightSpike;
    this.isBloom = isBloom;
  }

  public CactusFlower(float price, String forma, double heightSpike, boolean isBloom) {
    this.setPrice(price);
    this.forma = forma;
    this.heightSpike = heightSpike;
    this.isBloom = isBloom;
  }

  public String getForma() {
    return forma;
  }

  public void setForma(String forma) {
    this.forma = forma;
  }

  public double getHeightSpike() {
    return heightSpike;
  }

  public void setHeightSpike(double heightSpike) {
    this.heightSpike = heightSpike;
  }

  public boolean isBloom() {
    return isBloom;
  }

  public void setBloom(boolean bloom) {
    isBloom = bloom;
  }

  @Override
  public double getPrice() {
    return super.getPrice();
  }

  @Override
  public void setPrice(double price) {
    super.setPrice(price);
  }

  @Override
  public String toString() {
    return "CactusFlower{"
        + "price="
        + super.getPrice()
        + ", forma='"
        + forma
        + '\''
        + ", heightSpike="
        + heightSpike
        + ", isBloom="
        + isBloom
        + '}';
  }
}
