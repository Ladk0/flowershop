package store;

import store.Model.BasketDecorator;
import store.Model.Item;
import store.Model.PaperDecorator;
import store.Model.RibbonDecorator;

public class Manager {
  Store store = new Store();

  public static void main(String[] args) {

    Item bouquet = new FlowerBouquet();
    bouquet.setPrice(5.0);
    System.out.println(bouquet);
    System.out.println(bouquet.getPrice());
    //        ((FlowerBouquet) bouquet).addFlower();
    bouquet = new PaperDecorator(bouquet);
    System.out.println(bouquet);
    System.out.println(bouquet.getPrice());

    bouquet = new BasketDecorator(bouquet);
    System.out.println(bouquet);
    System.out.println(bouquet.getPrice());

    bouquet = new RibbonDecorator(bouquet);
    System.out.println(bouquet);
    System.out.println(bouquet.getPrice());

    CactusFlower cactusFlower = new CactusFlower("big", 10, true);
    cactusFlower.setPrice(11);
    System.out.println(cactusFlower);
  }
}
