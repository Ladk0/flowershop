package store.Model;

public class RibbonDecorator extends ItemDecorator  {
  private double ribbonPrice = 4;
  private double ribbonLength;

  public RibbonDecorator(Item item) {
    super(item);
  }

  public RibbonDecorator(Item item, double ribbonPrice, double ribbonLength) {
    super(item);
    this.ribbonPrice = ribbonPrice;
    this.ribbonLength = ribbonLength;
  }

  public double getRibbonPrice() {
    return ribbonPrice;
  }

  public void setRibbonPrice(double ribbonPrice) {
    this.ribbonPrice = ribbonPrice;
  }

  public double getRibbonLength() {
    return ribbonLength;
  }

  public void setRibbonLength(double ribbonLength) {
    this.ribbonLength = ribbonLength;
  }

  @Override
  public double getPrice() {
    return item.getPrice() + getRibbonPrice();
  }
}
