package store.Model;

public class BasketDecorator extends ItemDecorator {
  private double basketPrice = 4;
  private int basketItemSize;

  public BasketDecorator(Item item) {
    super(item);
  }

  public BasketDecorator(Item item, double basketPrice, int basketItemSize) {
    super(item);
    this.basketPrice = basketPrice;
    this.basketItemSize = basketItemSize;
  }

  public double getBasketPrice() {
    return basketPrice;
  }

  public void setBasketPrice(double basketPrice) {
    this.basketPrice = basketPrice;
  }

  public int getBasketItemSize() {
    return basketItemSize;
  }

  public void setBasketItemSize(int basketItemSize) {
    this.basketItemSize = basketItemSize;
  }

  @Override
  public double getPrice() {
    return super.getPrice() + getBasketPrice();
  }

  @Override
  public String toString() {
    return "BasketDecorator{"
        + "basketPrice="
        + basketPrice
        + ", basketItemSize="
        + basketItemSize
        + '}';
  }
}
