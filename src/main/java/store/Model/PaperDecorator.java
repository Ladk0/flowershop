package store.Model;


public class PaperDecorator extends ItemDecorator  {

  private double paperPrice = 13.0;
  private PaperColor paperColor;

  public PaperDecorator(Item item) {
    super(item);
  }

  public PaperDecorator(Item item, double paperPrice, PaperColor paperColor) {
    super(item);
    this.paperPrice = paperPrice;
    this.paperColor = paperColor;
  }

  public double getPaperPrice() {
    return paperPrice;
  }

  public void setPaperPrice(double paperPrice) {
    this.paperPrice = paperPrice;
  }

  public PaperColor getPaperColor() {
    return paperColor;
  }

  public void setPaperColor(PaperColor paperColor) {
    this.paperColor = paperColor;
  }

  @Override
  public double getPrice() {
    return super.getPrice() + getPaperPrice();
  }

    @Override
    public String toString() {
        return "PaperDecorator{" +
                "paperPrice=" + paperPrice +
                ", paperColor=" + paperColor +
                '}';
    }
}
