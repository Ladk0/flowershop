package store.Model;

public abstract class ItemDecorator extends Item {
  Item item;

  public ItemDecorator(Item item) {
    this.item = item;
  }

  @Override
  public String getDescription() {
    return item.getDescription();
  }

  @Override
  public double getPrice() {
    return item.getPrice();
  }

    @Override
    public String toString() {
        return "ItemDecorator{" +
                "item=" + item +
                '}';
    }
}
