package store;

import store.Model.Item;

public class RomashkaFlower extends Item {
  private boolean isBloom;

  public RomashkaFlower(boolean isBloom) {
    this.isBloom = isBloom;
  }

  public boolean isBloom() {
    return isBloom;
  }

  public void setBloom(boolean bloom) {
    isBloom = bloom;
  }

  @Override
  public void setPrice(double price) {
    super.setPrice(price);
  }

  @Override
  public double getPrice() {
    return super.getPrice();
  }

  @Override
  public String toString() {
    return "RomashkaFlower{" + "price=" + super.getPrice() + ", isBloom=" + isBloom + '}';
  }
}
