package store.Delivery;

import store.Model.Item;

import java.util.List;

public class PostDeliveryStrategy implements Delivery {
    @Override
    public void deliver(List<Item> products) {
        System.out.println("Starting delivery by Post");
    }
}
