package store.Delivery;

import store.Model.Item;

import java.util.List;

public interface Delivery {
    void deliver(List<Item> products);
}
